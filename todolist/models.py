from django.db import models
from django.utils.translation import ugettext as _


class Group(models.Model):
    title = models.CharField(_('Title'), max_length=256)


class Task(models.Model):
    STATUS_PENDING = 'P'
    STATUS_DONE = 'D'
    STATUS_CHOICES = (
        ('P', 'Pending'),
        ('D', 'Done'),
    )
    title = models.CharField(_('Title'), max_length=256)
    created = models.DateTimeField(_('Created at'), auto_now=True)
    status = models.CharField(_('Status'), max_length=1, default=STATUS_PENDING, choices=STATUS_CHOICES)
    group = models.OneToOneField(Group, related_name='tasks', on_delete=models.PROTECT)
